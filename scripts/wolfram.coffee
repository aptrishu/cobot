# Description:
#   Allows hubot to answer almost any question by asking Wolfram Alpha
#
# Dependencies:
#   "node-wolfram": ""
#
# Configuration:
#   HUBOT_WOLFRAM_APPID - your AppID
#
# Commands:
#   hubot <wa|wolfram> <question> - Searches Wolfram Alpha for the answer to the question
#
# Author: 
#   ChauffeR
Client = require 'node-wolfram'
Wolfram = new Client(process.env.HUBOT_WOLFRAM_APPID)

module.exports = (robot) ->
  robot.respond /(wolfram|wa) (.*)$/i, (msg) ->
    Wolfram.query msg.match[2], (err, result) ->
      if err?
        console.log err
        msg.send "error: error"
      else
        textmsg = ''
        for pod in result.queryresult.pod
          if pod.$.title in ['Result', 'Results']
            for subpod in pod.subpod
              for textpla in subpod.plaintext
                textmsg += textpla + "\n"
        if not textmsg
          msg.send "dunno"
        else
          msg.send textmsg

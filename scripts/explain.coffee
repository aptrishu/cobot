# Description:
#   Allows hubot to answer almost any question by consulting our own encyclopedia
#
# Commands:
#   hubot explain <topic> - Returns the info we have on your topic
#
# Author:
#   underyx
messages =
  explain: undefined
  closes: "We use bug prediction in coala which relies on the `Fixes` keyword in commit messages. To get good results from that we need to use `Closes` for normal issues instead of `Fixes` which should only be used for real bugs. (See also [the commit message docs](https://coala.io/commit).) To change your message you just use `git commit --amend` and then `git push --force` the new commit to replace the old one."
  fixes: "We use bug prediction in coala which relies on the `Fixes` keyword in commit messages. To get good results from that we need to use `Fixes` for bugfix issues instead of `Closes`. (See also [the commit message docs](https://coala.io/commit).) To change your message you just use `git commit --amend` and then `git push --force` the new commit to replace the old one."
  "commit message": "To change your message you just use `git commit --amend` and then `git push --force` the new commit to replace the old one.\n\nIf you're just looking to fix an issue very quickly and not interested in contributing to coala long term, we can fix up the message for you - just tell us :)."
  rebase: "It looks like your PR is out of date and needs a rebase.\n\n[This page](https://coala.io/git) may help you to get started on this. We also have [a quick video tutorial on how to rebase](https://asciinema.org/a/78683). That should help you understand the basics of how it works and what you should be doing.\n\nIf you're just looking to fix an issue very quickly and not interested in contributing to coala long term, we can fix it up for you - just tell us :)."
  cep: "At coala we're using [cEP's (coala Enhancement Proposals)](http://coala.io/cep) to define major design decisions - they're a bit like PEP's but not quite as extensive and obviously written with a lower case c."

topic_list_items = for topic, message of messages
  " - #{topic}"
messages.explain = ("Run 'cobot explain <keyword>' to get a reply from me like this one. I know about the following topics:\n\n" + topic_list_items.join("\n"))

module.exports = (robot) ->
  robot.respond /explain (.*)$/i, (msg) ->
    response = messages[msg.match[1].toLowerCase()]
    if !response
      msg.send "Sorry, I only know about these things:\n\n" + topic_list_items.join("\n")
    else
      msg.send response

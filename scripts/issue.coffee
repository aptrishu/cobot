# Description:
#   Allows hubot to file a issue on a particular repo given the name of the repo
#   the desired title, and description of the issue to be filed.
#
# Author:
#   Meet Mangukiya (@meetmangukiya)
#
# Commands:
#   hubot new issue - Creates a GitHub issue given the title and repository
#
# Configuration:
#   HUBOT_GITHUB_OAUTH
#   HUBOT_GITHUB_ORGNAME
#   HUBOT_NAME

accessToken = process.env.HUBOT_GITHUB_OAUTH
username = process.env.HUBOT_GITHUB_ORGNAME
gitterToken = process.env.HUBOT_GITTER2_TOKEN

module.exports = (robot) ->
    robot.respond /(?:new|file) issue$/i, (res) ->
        res.send "The correct syntax for filing an issue is\n" +
                 "```\n#{robot.name} file issue <repository> <title>\n" +
                 "[description]\n```"

    robot.respond /(?:new|file) issue ([\w-]+?)(?: |\n)(.+?)(?:$|\n((?:.|\n)*))/i, (res) ->
        repoName = res.match[1]
        issueTitle = res.match[2]
        if res.match[3] is undefined
            issueDescription = ""
        else
            issueDescription = res.match[3]
        robot.http("https://api.gitter.im/v1/rooms/#{res.message.room}")
            .header('Content-Type', 'application/json')
            .header('Accept', 'application/json')
            .header('Authorization', "Bearer #{gitterToken}")
            .get() (err, response, body) ->
                roomURI = JSON.parse(body)["uri"]
                issueDescription += "\n\nOpened via [gitter](https://gitter.im/#{roomURI}/?at=#{res.message.id}) by @#{res.message.user.login}"
                data = {
                    "title": issueTitle,
                    "body": issueDescription
                }
                robot.http("https://api.github.com/repos/#{username}/#{repoName}/issues")
                    .header('Content-Type', 'application/json')
                    .header('Authorization', "token #{accessToken}")
                    .post(JSON.stringify(data)) (err, response, body) ->
                        if response.statusCode is not 201 or err
                            if err is not undefined
                                res.send "Issue filing failed :( Error:  #{err}"
                            else
                                res.send "Issue filing failed :( Status Code: #{statusCode}"
                            return
                        else
                            linkToIssue = JSON.parse(body)["html_url"]
                            res.send "Here you go: #{linkToIssue}"
                            return
